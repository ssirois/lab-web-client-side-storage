# Client-Side Storage

[<abbr title="Mozilla Developers Network">MDN</abbr> Client-side storage](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Client-side_web_APIs/Client-side_storage)

- Cookies
- Web Storage
- IndexedDB
- Cache
- Origin Private File System (OPFS)

## Quotas

[<abbr title="Mozilla Developers Network">MDN</abbr> Storage quotas and eviction criteria](https://developer.mozilla.org/en-US/docs/Web/API/Storage_API/Storage_quotas_and_eviction_criteria)


### Cookies

### Web Storage

> Browsers can store up to 5 MiB of local storage, and 5 MiB of session
> storage per origin.

### IndexedDB, Cache, or File System (which defines OPFS)

Those APIs are managed by a storage management system that's specific
to each browser.

Ask for it:
[`navigator.storage.estimate()`](https://developer.mozilla.org/en-US/docs/Web/API/StorageManager/estimate)

- [Experimentation: estimate web storage quota](experimentations/estimate-web-storage-quota/index.hml)
  - [Estimate web storage quota results](#estimate-web-storage-quota-results)

#### Firefox

There is a *best-effort* mode and a *persistent mode*.

**Best-effort mode** quota is whichever is the smaller of:

- 10% of the total disk size where the profile of the user is stored.
- Or 10 GiB, which is the group limit that Firefox applies to all origins
that are part of the same eTLD+1 domain.

Origins can ask and be granted a *persistent storage*.

**Persistent storage mode** quota can store up to 50% of the total disk
size, capped at 8TiB, and are not subject to the eTLD+1 group limit.

> Note that it might not actually be possible for the origin to reach
> its quota because it is calculated based on the hard drive total size,
> not the currently available disk space. This is done for security
> reasons, to avoid fingerprinting.

#### Chrome and Chromium-based browsers

An origin can store up to 60% of the total disk size in both persistent
and best-effort modes.

#### Safari

Starting with macOS 14 and iOS 17, Safari allows around 20% of the total disk space for each origin.

If the origin is saved as a *web app* on the *Home Screen* or the *Dock*,
limit is increased to up to 60% of the disk size.

#### Estimate Web Storage Quota Results

![Screenshot of experiment ran under Firefox and Chromium in both normal and private modes](assets/estimate-web-storage-quota_firefox-chrome_normal-and-private-mode.png)

Screenshot displays:

- Linux 4.19.0-26-amd64 #1 SMP Debian 4.19.304-1 (2024-01-09) x86_64
GNU/Linux
- Debian GNU/Linux 10 (buster)
- Firefox version 115.8.0esr (64-bit)
- Google Chrome version 122.0.6261.69 (Official Build) (64-bit)

##### Observations

The experiment confirms close enough the expected quotas based on
reading notes.

Both browsers offer ±10<abbr title="Giga-Bytes">GB</abbr> of web storage
space without any permissions.

-----

There is a big discrepancy on the available space in Chrome
between standard and private mode. In private mode: only
1.83<abbr title="Giga-Bytes">GB</abbr> are available.

# File System API

[<abbr title="Mozilla Developers Network">MDN</abbr> File System API](https://developer.mozilla.org/en-US/docs/Web/API/File_System_API)

> Secure context: This feature is available only in secure contexts
> (HTTPS), in some or all supporting browsers.

## Observations

Under Firefox, on `file:` scheme to run experimentation,
`window.showOpenFilePicker` is not a function. This seems to fit
the documented behavior that the feature is only available in
[secure contexts](https://developer.mozilla.org/en-US/docs/Web/Security/Secure_Contexts)
(HTTPS).

Same behavior when served from the `https:` scheme.

🤦

Reading
[Window: showOpenFilePicker() Browser compatibility](https://developer.mozilla.org/en-US/docs/Web/API/Window/showOpenFilePicker#browser_compatibility)
section would have expedite tests under the Firefox browser.


As of typing this, this feature is only available for:

- Chrome 86 (Released 2020-10-20)
- Edge 86   (Released 2020-10-09)
- Opera 72  (Released 2020-10-21)

Not even experimental on any other browser, i.e. tagged as `No support`.

-----

Under Chrome, on `file:` scheme to run experimentation,
`window.showOpenFilePicker` throws a `DOMException`:

Failed to execute 'showOpenFilePicker' on 'Window': Must be handling a
user gesture to show a file picker.

Listening to the error message, call to `window.showOpenFilePicker`
is now triggered from a button click event (a user gesture). The file
picker is displayed.

It works.

![Screenshot of experiment ran under Chrome](assets/local-file-content_success-in-chrome.png)
